function [sekwencje,ZaznaczonaV] = sciezkaPodobLokal(genI,genII,V)
sekw1='';
sekw2='';
znacznikiPodob='';
[iWymiarMacierzy,jWymiarMacierzy]=find(V==max(V(:)),1,'last');
[poczatekKoniecGen1(2),poczatekKoniecGen2(2)]=find(V==max(V(:)),1,'last');
wartoscPomocnicza1='';
wartoscPomocnicza2='';
liczbaPomocnicza=[];
ZaznaczonaV=V;
while V(iWymiarMacierzy,jWymiarMacierzy)>0 && ((iWymiarMacierzy>1)||(jWymiarMacierzy>1))
wartoscPomocnicza1='';
wartoscPomocnicza2='';
ZaznaczonaV(iWymiarMacierzy,jWymiarMacierzy)=2*max(V,[],'all');
   if (iWymiarMacierzy>0)&&(jWymiarMacierzy>0)
       wartoscPomocnicza1=genI(iWymiarMacierzy-1);
       wartoscPomocnicza2=genII(jWymiarMacierzy-1);
       liczbaPomocnicza=V(iWymiarMacierzy-1,jWymiarMacierzy-1);
       if V(iWymiarMacierzy-1,jWymiarMacierzy)>liczbaPomocnicza
           liczbaPomocnicza=V(iWymiarMacierzy-1,jWymiarMacierzy);
           wartoscPomocnicza2='-';
       end
       if V(iWymiarMacierzy,jWymiarMacierzy-1)>liczbaPomocnicza
           wartoscPomocnicza1='-';
           wartoscPomocnicza2=genII(jWymiarMacierzy-1);
       end
   elseif iWymiarMacierzy==1
       wartoscPomocnicza1='-';
       wartoscPomocnicza2=genII(jWymiarMacierzy-1);
   else 
       wartoscPomocnicza1=genI(iWymiarMacierzy-1);
       wartoscPomocnicza2='-';
   end
   
   sekw1=[wartoscPomocnicza1,sekw1];
   sekw2=[wartoscPomocnicza2,sekw2];
   
   if wartoscPomocnicza1=='-'
       jWymiarMacierzy=jWymiarMacierzy-1;
   elseif wartoscPomocnicza2=='-'
       iWymiarMacierzy=iWymiarMacierzy-1;
   else
       jWymiarMacierzy=jWymiarMacierzy-1;
       iWymiarMacierzy=iWymiarMacierzy-1;
   end
   poczatekKoniecGen1(1)=iWymiarMacierzy;
   poczatekKoniecGen2(1)=jWymiarMacierzy;
end

for i=1:(length(sekw1))
    if sekw1(i)==sekw2(i)
        znacznikiPodob=[znacznikiPodob,'|'];
    else
        znacznikiPodob=[znacznikiPodob,'x'];
    end
end

sekwencje=struct("sekwencja1",sekw1,"znacznikiPodobienstwa",znacznikiPodob,"sekwencja2",sekw2,"tryb",'local similarity',"punktacja",max(V,[],'all'),"dlugosc",length(sekw1),"zgodnosci",length(strfind(znacznikiPodob,'|')),"przerwy",length(strfind(sekw1,'-'))+length(strfind(sekw2,'-')),"poczatekKoniec1",poczatekKoniecGen1,"poczatekKoniec2",poczatekKoniecGen2);

end