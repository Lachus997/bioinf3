function fastaStru = fastaParser(Identyfikator, Fasta)
remain = char(Fasta);
fastaStru = struct('Identyfikator',{},'Etykieta',{},'Sekwencja',{});
i = 0;
while ~isempty(remain)
    [token,remain] = strtok(remain,newline);
    if ~isempty(token) && token(1) == '>'
        i=i+1;
        fastaStru(i).Identyfikator=Identyfikator;
        fastaStru(i).Etykieta = token(2:end);
        fastaStru(i).Sekwencja = '';
    elseif i == 1
        fastaStru(1).Sekwencja = strcat(fastaStru(1).Sekwencja, token);
    end
end
fastaStru(1).Sekwencja = char(fastaStru(1).Sekwencja);
end